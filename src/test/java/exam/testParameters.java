package exam;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class testParameters {

	int n;
	exam2 exam2;
	
	@Parameters
	public static Collection<Object[]> numbers() {
		return Arrays.asList(new Object[][] {
			{0},
			{1},
			{9}
		});
	}
	
	public testParameters(int n) {
		this.n = n;
	}
	
	@Before
	public void setUp() {
		exam2 = new exam2();
	}
	
	@Test
	public void testExam() {
		assertTrue(exam2.exam(n));
	}

}
